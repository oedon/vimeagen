# Commands
## Help
:h <command>
:options

# Git
:Git (s for stage)
:Git status
:Git commit

git reset --hard (reset local changes)
git pull (pull remote rep)
git fetch (sync remote rep)
# Bash
## Source the current file
:source %

# Remaps
mode lhs rhs
n=normal
i=insert
v=visual

nnoremap <leader>KEY :function

# Keybinds
## Register
:reg Show table
""y yank first register
""p paste first register

## Random Stuff
ga Show ASCII on cursor

# Global
:help keyword   # open help for keyword
:o file         # open file
:saveas file    # save file as
:close          # close current pane
:sh             # go to terminal which started vim (type exit to go back to vim)
:terminal       # open new terminal in horizontal split
.               # repeat last command
u               # undo
^r              # redo

# Command Structure
[count] {operator} {[count] motion|text object}

## Operators that needs motions or text objects or other commands
v          # Visually select
V          # Visually select entire line
^v         # Visually select block
y          # Yank
d          # Delete (cut)
c          # Change
gu         # Change to lowercase
gU         # Change to uppercase
g~         # Switch case

## Text Objects
{special motion} {object}

# Special Motion
a          # all (with whitespace)
i          # in  (without whitespace)

# Object
w              # words
s              # sentences
p              # paragraphs
t              # tags
'|"|`          # quote
[]|{}|()|<>    # block
b              # For () block
B              # For {} block

# Motions
## Cursor
h          # move cursor left
j          # move cursor down
k          # move cursor up
l          # move cursor right
H          # move cursor to top of screen
M          # move cursor to middle of screen
L          # move cursor to bottom of screen
## World
w          # jump forwards to the start of a word
W          # jump forwards to the start of a word (words can contain punctuation)
e          # jump forwards to the end of a word
E          # jump forwards to the end of a word (words can contain punctuation)
b          # jump backwards to the start of a word
B          # jump backwards to the start of a word (words can contain punctuation)
ge         # jump backwards to the end of a word
gE         # jump backwards to the end of a word (words can contain punctuation)
## Line
0          # jump to the start of the line
^          # jump to the first non-blank character of the line (Shift + 6)
$          # jump to the end of the line
g_         # jump to the last non-blank character of the line
## Block
%          # jump to next parenthesis or bracket
}          # jump to next paragraph (or function/block, when editing code)
{          # jump to previous paragraph (or function/block, when editing code)
(          # jump forward one sentence
)          # jump backward one sentence
[(         # jump cursor to previous available parenthesis
])         # jump cursor to next available parenthesis
[{         # jump cursor to previous available bracket
]}         # jump cursor to next available bracket
## Other
gg         # go to the first line of the document
<number>gg # go to line given by the number
G          # go to the last line of the document
<number>G  # go to line given by the number

gd         # go to the local declaration
gD         # go to the global declaration
g*         # search for word contains the word under the cursor
*          # search for word under the cursor
g#         # g* backward
(#)          # * backwards
## Marker
mx         # mark 'x' at the current cursor position

'x         # jump to the beginning of the line of mark 'x'
`x         # jump to the cursor position of mark 'x'
## Undo
''         # return to the line where the cursor was before the latest jump (Two single quotes.)
``         # return to the cursor position before the latest jump (undo the jump) (Two back ticks)
`.         # jump to the cursor location of the last-changed line
'.         # jump to the begining of the last-changed line

^o         # undo the last jump

# Page Scrolls
^e         # Scroll the window down
^y         # Scroll the windows up
^f         # Scroll down one page (forward scroll)
^b         # Scroll up one page (backward scroll)
^d         # Scroll down 1/2 page
^u         # Scroll up 1/2 page
zt         # Scroll to get cursor to screen top
zz         # Scroll to get cursor to screen center
zb         # Scroll to get cursor to screen bottom

# Macro
q{register}          # Start Recording
--do commands to record--
q                    # Stop recording

@{register}          # Play recording in the register
@@                   # Play the previously played register

:reg                 # See values of every register

# Inser mode
i        # insert before the cursor
I        # insert at the beginning of the line
a        # insert (append) after the cursor
A        # insert (append) at the end of the line
o        # append (open) a new line below the current line
O        # append (open) a new line above the current line
Esc      # goto normal mode
^o       # temporary goto normal mode from insert mode
^r{reg}  # print the value of the register {reg}

# Editing
i        # insert before the cursor
I        # insert at the beginning of the line
a        # insert (append) after the cursor
A        # insert (append) at the end of the line
o        # append (open) a new line below the current line
O        # append (open) a new line above the current line
Esc      # goto normal mode
^o       # temporary goto normal mode from insert mode
^r{reg}  # print the value of the register {reg}

# Cut and paste
yy|Y        # yank (copy) [number] of lines
p           # put (paste) the clipboard after cursor
P           # put (paste) before cursor
dd          # delete (cut) [number] of lines
D           # delete (cut) to the end of the line (same as d$)
x           # delete (cut) character

# Visual mode
v        # start visual mode, mark lines, then do a command (like y-yank)
V        # start linewise visual mode
^v       # start visual block mode
o        # move to other end of marked area
O        # move to other corner of block
Esc      # goto normal mode
# //TODO
https://www.fcodelabs.com/2018/12/08/Vim-Cheats/
https://www.overleaf.com/learn/latex/Headers_and_footers#Style_customization_in_single-sided_documents
# Code Changes
Plug 'raimondi/delimitmate'
" Markdown
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
" Beancount
Plug 'nathangrigg/vim-beancount'
" Buffer
nnoremap <leader><TAB> :bn<cr>
nnoremap <TAB> za
autocmd BufWinLeave *.beancount mkview
autocmd BufWinEnter *.beancount silent loadview

" Buffer
nnoremap <TAB> :bn<cr>
"nnoremap <TAB> za
autocmd FileType beancount map <buffer> <TAB> za
