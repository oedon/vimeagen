# Config changes
- texlab Server

```
lua << EOF
require'lspconfig'.texlab.setup{
    cmd = {"/home/oedon/Downloads/texlab"}
}
EOF
```

" Color
Plug 'chrisbra/colorizer'


# Vim Stuff
- Ctrl-n completion inside vim
- Ctrl-p completion search
- :Explorer vim file explorer :Ex :Lex
- :Colortoggle for hex color
- Ctrl-V -> I Insert on vertical row on ESC
- gf go to file under cursor



# TODO
- Comments on vim config
- https://github.com/ChristianChiarulli/LunarVim#why-do-i-want-tree-sitter-and-lsp
- https://jrnl.sh/en/stable/overview/
- https://www.youtube.com/watch?v=gZCXaF-Lmco
- https://www.chrisatmachine.com/Neovim/02-vim-general-settings/
- treesitter
- https://github.com/folke/which-key.nvim
- https://github.com/TimUntersberger/neogit
- https://github.com/wbthomason/packer.nvim
- https://github.com/kyazdani42/nvim-tree.lua
